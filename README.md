
# DOCKER TOMCAT LINKED TO POSTGRESQL BDD


## WEB-CHAUSSURE:

### Créer Dockerfile:

      FROM tomcat:8-jre8
      RUN apt-get update
      RUN apt-get install -y git \
                             nano \
                             postgresql-9.5
      MAINTAINER Arthur Girard
      COPY ./dbproject.war /usr/local/tomcat/webapps/
      WORKDIR /usr/local/tomcat/webapps/

### Contruction serveur tomcat

      docker build -t web-chaussure .

### Lancement du serveur tomcat

      docker run -d --name web-chaussure -p 9000:8080 --link db web-chaussure

## BDD

### Créer Dockerfile:

      FROM postgres:9.5
      COPY /init-db.sql /docker-entrypoint-initdb.d/
      VOLUME dbdata:/var/lib/postgresql/data

### Construction serveur bdd

      docker build -t bdd-chaussure .

### Lancement du serveur bdd

      docker run -d --name db -v /dbdata:/var/lib/postgresql/data bdd-chaussure
      

## Contruction et lancement par docker-compose

### Creation du docker-compose.yml
                                      
      version: '3'                           
      services:                              
       webapp:                               
        build: ./docker                      
        ports:                               
         - "9000:8080"                       
        depends_on:                          
         - db                                

       db:                                   
        build: ./bdd                         
        volumes:                             
         - dbdata:/var/lib/postgresql/data   
      volumes:                               
       dbdata:                               
 ### Construction et lancement
 
      docker-compose up -d 
